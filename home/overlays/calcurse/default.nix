self: super:

# Caldav does not work in version 4.8.0

{
  calcurse = super.calcurse.overrideAttrs (
    oldAttrs: rec {
      pname = "calcurse";
      version = "4.7.1";

      src = super.fetchurl {
        url = "https://calcurse.org/files/${pname}-${version}.tar.gz";
        sha256 = "sha256-CnxV0HZ0Vp0WbAsOdYeyly09qBYM231gsdvSiVgDr7A=";
      };
    }
  );
}


