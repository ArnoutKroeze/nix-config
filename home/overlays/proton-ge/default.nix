self: super:

{
  protonup = super.protonup.overrideAttrs (
    oldAttrs: rec {
      version = "0.2.1";

      src = super.fetchFromGitHub {
        owner = "cloudishBenne";
        repo = "protonup-ng";
        rev = "390d31d79a7324b6015334a669b0f65695bd9c61";
        sha256 = "sha256-hShF08nwYU0XDFZrFhxJ7MswkhbE/YJmx1n9jiGsonk=";
      };

    }
  );
}


