self: super:

{
  polybar = super.polybar.overrideAttrs (
    oldAttrs: rec {
      version = "3.6.3";

      src = super.fetchFromGitHub {
        owner = "mihirlad55";
        repo = "polybar-dwm-module";
        rev = "0c3e139ac54e081c06ef60548927e679d80d4297";
        sha256 = "sha256-ZL7yDGGiZFGgVXZXS+d3xUDhOOd5lp2mo2ipFN92mIY=";
        fetchSubmodules = true;
      };

      nativeBuildInputs = oldAttrs.nativeBuildInputs ++ [super.git];
      buildInputs = oldAttrs.buildInputs ++ [super.jsoncpp];
      patches = [];
      postPatch = "";

    }
  );
}


