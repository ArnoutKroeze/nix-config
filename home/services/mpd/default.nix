{ pkgs, ... }:
{

  # so playerctl can see mpd
  services.mpdris2.enable = true;

  services.mpd = {
    enable = true;
    musicDirectory = "/media/muziek";

    network = {
      listenAddress = "0.0.0.0";
      # different port from default since 6600 didnt seem to work 
      port = 6600;
      startWhenNeeded = false;
    };

    extraConfig = ''
      audio_output {
        type "pulse"
        name "pulse"
      }

      audio_output {
        type	"fifo"
        name	"my_fifo"
        path	"/tmp/mpd.fifo"
        format	"44100:16:2"
      }
    '';
  };
  home.sessionVariables = {
    MPD_HOST = "0.0.0.0";
  };
}

