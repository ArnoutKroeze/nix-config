{ pkgs, ... }:

{

  services.sxhkd = {
    enable = true;
    keybindings = {
      # F keys, mainly some fun

      "super + F4" = "st -e pulsemixer;";

      # qwerty

      "super + w" = "firefox";
      "super + e" = "keepassxc";
      "super + shift + e" = "thunderbird";
      "super + r" = "st -e ranger";
      "super + y" = "freetube";
      "super + p" = "mpc toggle";
      "super + shift + p" = "playerctl play-pause -a";
      "super + bracketleft" = "mpc seek -10";
      "super + shift + bracketleft" = "mpc seek -60";
      "super + bracketright" = "mpc seek +10";
      "super + shift + bracketright" = "mpc seek +60";

      # asdf
      "super + a" = "st -e calcurse";
      "super + s" = "steam";
      "super + d" = "dmenu_run";
      "super + shift + d" = "discord";



      # zxcv

      "super + x" = "clipmenu";
      "super + c" = "element-desktop";
      "super + n" = "st -e newsboat";
      "super + m" = "st -e ncmpcpp";

      "super + shift + m" = "pamixer -t";
      "super + comma" = "mpc prev";
      "super + shift + comma" = "mpc seek 0%";
      "super + period" = "mpc next";
      "super + shift + period" = "mpc repeat";


      # special
      "super + grave" = "dmenuunicode";


      "super + minus" = "pamixer --allow-boost -d 5";
      "super + shift + minus" = "mpc volume -5";
      "super + equal" = "pamixer --allow-boost -i 5";
      "super + shift + equal" = "mpc volume +5";

      "super + BackSpace" = "slock";

      "Print" = "flameshot gui";
      "super + Return" = "st";
      "super + KP_Enter" = "st";
      "super + KP_subtract" = "pamixer -d 5 --allow-boost";
      "super + KP_Add" = "pamixer -i 5 --allow-boost  ";

      "XF86AudioMute" =  "pamixer -t";
      "XF86AudioLowerVolume" = "pamixer --allow-boost -d 5";
      "XF86AudioRaiseVolume" = "pamixer --allow-boost -i 5";
      "XF86Search" = "dmenu_run";

      "XF86MonBrightnessUp" = "brightnessctl s +10% --min-value=5";
      "XF86MonBrightnessDown" = "brightnessctl s 10%- --min-value=5";
    };
  };
}
