{ pkgs, config, lib, ... }:

{
  services.polybar = {
    enable = true;

    package = pkgs.polybar.override {
      mpdSupport = true;
      pulseSupport = true;
    };

    config = ./config;

    # script = ''
    #   killall -q polybar
    #   for m in $(polybar --list-monitors | cut -d":" -f1); do
    #       MONITOR=$m polybar --reload mybar --log=trace&
    #   done
    # '';

    script = ''
      killall -q polybar

      for m in $(polybar --list-monitors | cut -d":" -f1); do
          MONITOR=$m polybar --reload mybar --log=trace&
      done
    '';
  };
}

