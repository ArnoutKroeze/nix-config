#!/usr/bin/env sh

killall -q polybar
for m in $(polybar --list-monitors | cut -d":" -f1); do
        # echo $m
  MONITOR=$m polybar --reload mybar --config=config.ini --log=trace&
done
