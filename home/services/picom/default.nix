{
  services.picom = {
    enable = true;
    # prevents screen glitches
    backend = "xrender";
    # activeOpacity = 0;
    # blur = true;
    # inactiveDim = "0.1";
    # vSync = false;
  };
}
