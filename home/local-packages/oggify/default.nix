{ lib
, fetchFromGitHub
, rustPlatform
}:

rustPlatform.buildRustPackage rec {
  pname = "oggify";
  version = "unstable-2020-12-28";

  src = fetchFromGitHub {
    owner = "Keschercode";
    repo = "oggify";
    rev = "ce3e8756d61adcb743d105041f832c6027d2e7f0";
    sha256 = "sha256-lONNFtGV3TZV863hs3FE58F38jl10dbJaIKI7DS0RvM=";
  };

  cargoSha256 = "sha256-NufWfJMK/1X/f1Tk/kQZ6jYdHJbuMTGjrz93tTdeFhg=";

  meta = with lib; {
    description = "A Spotify downloader written in rust";
    homepage = "https://github.com/kescherCode/oggify";
    license = licenses.mit;
    platforms = platforms.linux;
    maintainers = with maintainers; [ arnoutkroeze ];
  };
}
