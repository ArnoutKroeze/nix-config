with import <nixpkgs> {};
let
  mkScript = { name, version, file, env ? [ ] }:
    writeTextFile {
      name = "${name}-${version}";
      executable = true;
      destination = "/bin/${name}";
      text = ''
        for i in ${lib.concatStringsSep " " env}; do
          export PATH="$i/bin:$PATH"
        done

        exec ${bash}/bin/bash ${file} $@
      '';
    };

in mkScript {
  name = "bier";
  version = "0.1";
  file = ./bier;
  env = with self; [bash];
}
