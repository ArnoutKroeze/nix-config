{ stdenv
,dmenu
,ffmpeg
,substituteAll
}:

stdenv.mkDerivation {
  name = "screenshot";

  buildCommand = ''
    install -Dm755 $script $out/bin/screenshot
  '';

  script = substituteAll {
    src = ./screenshot.sh;
    isExecutable = true;
    inherit ffmpeg dmenu;
    inherit (stdenv) shell;
  };

  meta = with stdenv.lib; {
    description = "Script to extract RPM archives";
    platforms = platforms.all;
    license = licenses.gpl2;
    maintainers = with maintainers; [ abbradar ];
  };

}
