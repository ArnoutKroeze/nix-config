# -*- coding: utf-8 -*-
import os
import re
import sys
import importlib
import socket
import subprocess
from libqtile import qtile
from libqtile.config import Click, Drag, Group, KeyChord, Key, Match, Screen
from libqtile.command import lazy
from libqtile import layout, bar, widget, hook
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal
from typing import List  # noqa: F401from typing import List  # noqa: F401

import custom_widgets

def reload(module):
    if module in sys.modules:
        importlib.reload(sys.modules[module])

reload("scratchpad")
from scratchpad import keys_scratchpad, scratchpad

mod = "mod4"              # Sets mod key to SUPER/WINDOWS
terminal = "st"      # My terminal of choice
myBrowser = "firefox" # My terminal of choice

def window_to_other_screen():
    """ Move window right, loop back if is the most right screen"""

    def f(qtile):
        new_screen = (qtile.screens.index(qtile.current_screen) + 1) % 2
        new_group = qtile.screens[new_screen].group.name

        qtile.current_window.togroup((new_group))
    return f

def swap(c1, c2, group):
    i1 = group.clients.index(c1)
    i2 = group.clients.index(c2)
    group.clients[i1], group.clients[i2] = group.clients[i2], group.clients[i1]
    group.current_index = i2


def swap_up():
    def f(qtile):
        group = qtile.current_group
        current_window = qtile.current_window
        indx = group.windows.index(current_window)
        up_window = group.windows[ (indx + 1) % len(group.windows)]
        swap(current_window, up_window, group.layout)
    return f

def swap_down():
    def f(qtile):
        group = qtile.current_group
        current_window = qtile.current_window
        indx = group.windows.index(current_window)
        down_window = group.windows[ (indx + -1) % len(group.windows)]
        swap(current_window, down_window, group.layout)
    return f


keys = [
         ### The essentials
         Key([mod], "q",
             lazy.window.kill(),
             desc='Kill active window'
             ),
         Key([mod, "control"], "r",
             lazy.restart(),
             desc='Restart Qtile'
             ),
        ### Switch focus of monitors
         Key([mod], "Left",
             lazy.next_screen(),
             desc='Move focus to next monitor'
             ),
         Key([mod, "shift"], "Left",
             lazy.function(window_to_other_screen()),
             desc='Move window to other screen',
             ),
         Key([mod], "i",
             lazy.next_screen(),
             desc='Move focus to next monitor'
             ),
         Key([mod, "shift"], "i",
             lazy.function(window_to_other_screen()),
             desc='Move window to other screen',
             ),
         Key([mod], "Right",
             lazy.prev_screen(),
             desc='Move focus to prev monitor'
             ),
         Key([mod, "shift"], "Right",
             lazy.function(window_to_other_screen()),
             desc='Move window to other screen',
             ),
         Key([mod], "o",
             lazy.prev_screen(),
             desc='Move focus to prev monitor'
             ),
         Key([mod, "shift"], "o",
             lazy.function(window_to_other_screen()),
             desc='Move window to other screen',
             ),
        ### Window controls
         Key([mod], "j",
             lazy.layout.down(),
             desc='Move focus down in current stack pane'
             ),
         Key([mod], "k",
             lazy.layout.up(),
             desc='Move focus up in current stack pane'
             ),
         Key([mod, "shift"], "j",
             lazy.function(swap_down()),
             desc='Move windows down in current stack'
             ),
         Key([mod, "shift"], "k",
             lazy.function(swap_up()),
             desc='Move windows up in current stack'
             ),
         Key([mod], "h",
             lazy.layout.shrink_main(),
             desc='shrink master window'
             ),
         Key([mod], "l",
             lazy.layout.grow_main(),
             desc='grow master window'
             ),
         Key([mod], "n",
             lazy.layout.normalize(),
             desc='normalize window size ratios'
             ),
        Key([mod, "shift"], "f",
             lazy.window.toggle_floating(),
             desc='toggle floating'
             ),
         Key([mod], "f",
             lazy.window.toggle_fullscreen(),
             desc='toggle fullscreen'
             ),
         Key([mod], "Return",
             lazy.spawn("st"),
             desc='Toggle through layouts'
             ),

]

keys += keys_scratchpad

def get_group_name(screen_no, group_no):
    return str(screen_no) + str(group_no)


def to_group(group):

    def f(qtile):
        cur_screen = qtile.screens.index(qtile.current_screen)
        new_group = get_group_name(cur_screen, group)
        qtile.groups_map[new_group].cmd_toscreen(toggle=False)
    return f

def window_to_group(group):

    def f(qtile):

        cur_screen = qtile.screens.index(qtile.current_screen)
        new_group = get_group_name(cur_screen, group)
        qtile.current_window.togroup((new_group))

    return f


groups = []
groups.append(scratchpad)
for screen_no in "01":
    for group_no in "123456789":
        groups.append(Group(
            name=screen_no+group_no,
            label=group_no))

    # create hotkeys to navigate to groups 

for i in "123456789":
    keys.extend([
        Key([mod], i, lazy.function(
                to_group(i)),
                desc="move to group"),

        Key([mod, "shift"], i, lazy.function(
            window_to_group(i)),
            desc="move window to group"),
        ])



# Allow MODKEY+[0 through 9] to bind to groups, see https://docs.qtile.org/en/stable/manual/config/groups.html
# MOD4 + index Number : Switch to Group[index]
# MOD4 + shift + index Number : Send active window to another Group

layout_theme = {"border_width": 2,
                "margin": 0,
                "border_focus": "e1acff",
                "border_normal": "1D2330"
                }


layouts = [
    #layout.MonadWide(**layout_theme),
    layout.MonadTall(**layout_theme),
    # layout.Bsp(**layout_theme),
    #layout.Stack(stacks=2, **layout_theme),
    #layout.Columns(**layout_theme),
    #layout.RatioTile(**layout_theme),
    #layout.Tile(shift_windows=True, **layout_theme),
    #layout.VerticalTile(**layout_theme),
    #layout.Matrix(**layout_theme),
    #layout.Zoomy(**layout_theme),
    # layout.Max(**layout_theme),
    # layout.Stack(num_stacks=2),
    # layout.RatioTile(**layout_theme),
   layout.Floating(**layout_theme)
]

colors = [["#282c34", "#282c34"], # panel background
          ["#3d3f4b", "#434758"], # background for current screen tab
          ["#ffffff", "#ffffff"], # font color for group names
          ["#ff5555", "#ff5555"], # border line color for current tab
          ["#74438f", "#74438f"], # border line color for 'other tabs' and color for 'odd widgets'
          ["#4f76c7", "#4f76c7"], # color for the 'even widgets'
          ["#e1acff", "#e1acff"], # window name
          ["#ecbbfb", "#ecbbfb"]] # backbround for inactive screens

prompt = "{0}@{1}: ".format(os.environ["USER"], socket.gethostname())

##### DEFAULT WIDGET SETTINGS #####


def bar_visible_groups(screen: str):
    result = []
    for group in groups:
        if group.name[0] == screen:
            result.append(group.name)

    return result

widget_defaults = dict(
    font="Ubuntu Mono",
    fontsize = 14,
    padding = 2,
    background=colors[2]
)
extension_defaults = widget_defaults.copy()

def init_widgets_list(screen_no: str):
    widgets_list = [
              widget.GroupBox(
                       font = "Ubuntu Bold",
                       fontsize = 14,
                       # margin_y = 3,
                       # margin_x = 0,
                       # padding_y = 5,
                       # padding_x = 3,
                       # borderwidth = 3,
                       active = colors[2],
                       inactive = colors[7],
                       hide_unused = True,
                       # rounded = False,
                       highlight_color = colors[1],
                       highlight_method = "box",
                       this_current_screen_border = colors[6],
                       this_screen_border = colors [4],
                       foreground = colors[2],
                       background = colors[0],
                       visible_groups = bar_visible_groups(screen_no)
                       ),
              widget.Sep(
                       linewidth = 0,
                       padding = 10,
                       foreground = colors[2],
                       background = colors[0]
                       ),
              # widget.WindowName(
              #          foreground = colors[2],
              #          background = colors[0],
              #          padding = 0,
              #          ),
             # widget.Sep(
             #           linewidth = 0,
             #           padding = 6,
             #           foreground = colors[0],
             #           background = colors[0]
             #           ),
              widget.TextBox(
                       text = '',
                       background = colors[5],
                       foreground = colors[4],
                       width = bar.STRETCH,
                       padding = 0,
                       fontsize = 37,
                       ),
              custom_widgets.Music(
                       markup=True,
                       status_format='{play_status} {artist}: {title}',
                       background = colors[4],
                       foreground = colors[2],
                       color_progress='e47969',
                       update_interval = 0.01,
                       padding = 5,
                       ),
              widget.TextBox(
                       text = '',
                       background = colors[4],
                       foreground = colors[5],
                       width = bar.STRETCH,
                       padding = 0,
                       fontsize = 37,
                       ),
              custom_widgets.Network(
                       interface = "enp4s0",
                       format = '{down} ↓↑ {up}',
                       foreground = colors[2],
                       background = colors[5],
                       padding = 5,
                       ),
                        # hoi
              widget.TextBox(
                       text = '',
                       background = colors[5],
                       foreground = colors[4],
                       padding = 0,
                       fontsize = 37,
                       ),
               widget.TextBox(
                       background = colors[4],
                       foreground = colors[4],
                       padding = 2,
                       fontsize = 0,
                       ),      
               widget.Volume(
                        update_interval = 0.01,
                        mouse_callbacks={'Button1': lambda: qtile.cmd_spawn(terminal + " -e pulsemixer"),
                                         'Button4': lambda: qtile.cmd_spawn("pamixer --allow-boost -i 5"),
                                         'Button5': lambda: qtile.cmd_spawn("pamixer --allow-boost -d 5")},
                       foreground = colors[2],
                       background = colors[4],
                       padding = 4,
                       ),
 
              widget.TextBox(
                       text = '',
                       background = colors[4],
                       foreground = colors[5],
                       padding = 0,
                       fontsize = 37,
                       ),
              custom_widgets.ClockRadian(
                       foreground = colors[2],
                       background = colors[5],
                       format = "%A, %B %d - ",
                       padding = 2,
                       ),
              ]
    return widgets_list

def init_widgets_screen1():
    widgets_screen1 = init_widgets_list("0")
    return widgets_screen1

def init_widgets_screen2():
    widgets_screen2 = init_widgets_list("1")
    return widgets_screen2                 # Monitor 2 will display all widgets in widgets_list

def init_screens():
    return [Screen(top=bar.Bar(widgets=init_widgets_screen1(), opacity=1.0, size=20)),
            Screen(top=bar.Bar(widgets=init_widgets_screen2(), opacity=1.0, size=20)),
            ]

if __name__ in ["config", "__main__"]:
    screens = init_screens()
    # widgets_list = init_widgets_list()
    widgets_screen1 = init_widgets_screen1()
    widgets_screen2 = init_widgets_screen2()

def window_to_prev_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i - 1].name)

def window_to_next_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i + 1].name)

def window_to_previous_screen(qtile):
    i = qtile.screens.index(qtile.current_screen)
    if i != 0:
        group = qtile.screens[i - 1].group.name
        qtile.current_window.togroup(group)

def window_to_next_screen(qtile):
    i = qtile.screens.index(qtile.current_screen)
    if i + 1 != len(qtile.screens):
        group = qtile.screens[i + 1].group.name
        qtile.current_window.togroup(group)

def switch_screens(qtile):
    i = qtile.screens.index(qtile.current_screen)
    group = qtile.screens[i - 1].group
    qtile.current_screen.set_group(group)

mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_app_rules = []  # type: List
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False

floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    # default_float_rules include: utility, notification, toolbar, splash, dialog,
    # file_progress, confirm, download and error.
    *layout.Floating.default_float_rules,
    Match(title='Confirmation'),      # tastyworks exit box
    Match(title='Qalculate!'),        # qalculate-gtk
    Match(wm_class='kdenlive'),       # kdenlive
    Match(wm_class='pinentry-gtk-2'), # GPG key password entry
])
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True

@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser('~')
    # subprocess.call([home + '/.config/qtile/autostart.sh'])

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
