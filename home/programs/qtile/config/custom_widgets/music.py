from libqtile.widget import Mpd2

from collections import defaultdict



class Music(Mpd2):

    def formatter(self, status, current_song): # noqa: N802
        """format song info."""
        default = 'Undefined'
        song_info = defaultdict(lambda: default)
        song_info['play_status'] = self.play_states[status['state']]

        if status['state'] == 'stop' and current_song == {}:
            song_info['idle_message'] = self.idle_message
            fmt = self.idle_format
        else:
            fmt = self.status_format

        for k in current_song:
            song_info[k] = current_song[k]
        song_info['fulltime'] = song_info['time']
        del song_info['time']

        song_info.update(status)
        if song_info['updating_db'] == default:
            song_info['updating_db'] = '0'
        if not callable(self.prepare_status['repeat']):
            for k in self.prepare_status:
                if k in status and status[k] != '0':
                    # Much more direct.
                    song_info[k] = self.prepare_status[k]
                else:
                    song_info[k] = self.space
        else:
            self.prepare_formatting(song_info)

        # 'remaining' isn't actually in the information provided by mpd
        # so we construct it from 'fulltime' and 'elapsed'.
        # 'elapsed' is always less than or equal to 'fulltime', if it exists.
        # Remaining should default to '00:00' if either or both are missing.
        # These values are also used for coloring text by progress, if wanted.
        if 'remaining' in self.status_format or self.color_progress:
            total = float(song_info['fulltime'])\
                if song_info['fulltime'] != default else 0.0
            elapsed = float(song_info['elapsed'])\
                if song_info['elapsed'] != default else 0.0
            song_info['remaining'] = "{:.2f}".format(float(total - elapsed))

        # mpd serializes tags containing commas as lists.
        for key in song_info:
            if isinstance(song_info[key], list):
                song_info[key] = ', '.join(song_info[key])

        # Now we apply the user formatting to selected elements in song_info.
        # if 'all' is defined, it is applied first.
        # the reason for this is that, if the format functions do pango markup.
        # we don't want to do anything that would mess it up, e.g. `escape`ing.

        """ deleted this and moved it such that it only escaped html stuff after the progress colouring.
        If formatted before, the code will try to colour only part of some escape sequences and break the widget"""

        # fmt = self.status_format
        if not isinstance(fmt, str):
            fmt = str(fmt)

        formatted = fmt.format_map(song_info)

        if self.color_progress and status['state'] != 'stop':
            try:
                progress = int(len(formatted) * elapsed / total)
                formatted = '<span color="{0}">{1}</span>{2}'.format(
                    self.color_progress, self.format_fns["all"](formatted[:progress]), self.format_fns["all"](formatted[progress:]),
                )
            except (ZeroDivisionError, ValueError):
                pass

        return formatted
