from libqtile.widget import GenPollText
import os

class Volume(GenPollText):

    def poll(self):
        text = os.popen("volume").read().strip()
        return text
