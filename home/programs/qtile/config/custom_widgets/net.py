from libqtile.widget import Net
from math import log
from typing import Tuple
from libqtile.log_utils import logger

class Network(Net):

    def convert_b(self, num_bytes: float) -> Tuple[float, str]:
        """Converts the number of bytes to the correct unit"""
        factor = 1000.0

        if self.use_bits:
            letters = ["b", "kb", "Mb", "Gb", "Tb", "Pb", "Eb", "Zb", "Yb"]
            num_bytes *= 8
        else:
            letters = ["B", "kB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"]

        if num_bytes > 0:
            power = int(log(num_bytes) / log(factor))
            power = max(min(power, len(letters) - 1), 0)
        else:
            power = 0

        converted_bytes = round(num_bytes / factor**power, 2)
        if power == 0:
            power = 1
        unit = letters[power]

        return converted_bytes, unit

    def _format(self, down, down_letter, up, up_letter):
        max_len_down = 7 - len(down_letter)
        max_len_up = 7 - len(up_letter)
        down = '{val:{max_len}.2f}'.format(val=down, max_len=max_len_down)
        up = '{val:{max_len}.2f}'.format(val=up, max_len=max_len_up)
        return down[:max_len_down], up[:max_len_up]

    def poll(self):
        ret_stat = []
        try:
            new_stats = self.get_stats()
            for intf in self.interface:
                down = new_stats[intf]['down'] - \
                    self.stats[intf]['down']
                up = new_stats[intf]['up'] - \
                    self.stats[intf]['up']

                down = down / self.update_interval
                up = up / self.update_interval
                down, down_letter = self.convert_b(down)
                up, up_letter = self.convert_b(up)
                # down, up = self._format(down, down_letter, up, up_letter)
                up = str(up).strip()
                down = str(down).strip()
                up = ("0" * (6 - len(up))) + up
                down = ("0" * (6 - len(down))) + down
                self.stats[intf] = new_stats[intf]
                ret_stat.append(
                    self.format.format(
                        **{
                            'interface': intf,
                            'down': down + down_letter,
                            'up': up + up_letter
                        }))

            return " ".join(ret_stat)
        except Exception as excp:
            logger.error('%s: Caught Exception:\n%s',
                         self.__class__.__name__, excp)

