from libqtile.utils import lazify_imports
from libqtile.widget.import_error import make_error

widgets ={
        "ClockRadian": "clockradian",
        "Music": "music",
        "Volume": "volume",
        "Wttr": "wttr",
        "Network": "net",
        }



__all__, __dir__, __getattr__ = lazify_imports(widgets, __package__,
                                               fallback=make_error)
