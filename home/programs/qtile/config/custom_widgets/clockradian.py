from libqtile.widget import Clock

from datetime import datetime, timezone

class ClockRadian(Clock):

    def poll(self):
        if self.timezone:
            now = datetime.now(timezone.utc).astimezone(self.timezone)
        else:
            now = datetime.now(timezone.utc).astimezone()
        hours = now.hour
        minute = now.minute

        hours = hours + 5*minute/300

        radial_time = round((4.5 - hours/6) % 4, 2)
        end_string = now.strftime(self.format)
        return end_string + str(radial_time) + "π"
