{ config, lib, pkgs, ... }:

let


in
{
  programs.zsh = {
    enable = true;
    enableAutosuggestions = true;
    enableCompletion = true;

    dirHashes = {
      d = "$HOME/documents";
      nixpkgs = "$HOME/documents/projects/nixpkgs";
    };

    initExtra = ''
      
      # pretty colors, and PS1
      autoload -U colors && colors
      PS1="%B%{$fg[red]%}[%{$fg[yellow]%}%n%{$fg[green]%}@%{$fg[blue]%}%M %{$fg[magenta]%}%~%{$fg[red]%}]%{$reset_color%}$%b";

      # vi mode
      bindkey -v
      bindkey -v '^?' backward-delete-char

      function zle-keymap-select () {
	    case $KEYMAP in
	        vicmd) echo -ne '\e[1 q';;      # block
	        viins|main) echo -ne '\e[5 q';; # beam
	    esac
      }

      zle -N zle-keymap-select
      zle-line-init() {
        zle -K viins
        echo -ne '\e[5 q'
      }
      zle -N zle-line-init
      echo -ne '\e[5 q'


      bindkey '^[[P' delete-char


      bindkey "^k" history-beginning-search-backward
      bindkey "^j" history-beginning-search-forward

      setopt prompt_subst
      . ${pkgs.git}/share/bash-completion/completions/git-prompt.sh
      export RPROMPT=$'$(__git_ps1 "%s")'
            '';
    
    shellAliases = {
      ls = "ls -hN --color=auto --group-directories-first";
      ll = "ls -l";
      la = "ls -A";
      lla = "ls -lA";
      e = "nvim";
      home-edit = "nvim ~/.config/nixpkgs/home.nix";
    };
  };
}



