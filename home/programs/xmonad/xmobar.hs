-- xmobar config

Config { font = "-*-Fixed-Bold-R-Normal-*-13-*-*-*-*-*-*-*",
         borderColor = "black",
	 allDesktops = True,
         border = TopB,
         bgColor = "black",
         fgColor = "grey",
         position = TopW L 100,
         commands = [  Run Network "eth0" ["-L","0","-H","32","--normal","green","--high","red"] 10
                      , Run Cpu ["-t", "<fn=2>\xf108</fn>  cpu: (<total>%)","-H","50","--high","red"] 20
                      , Run Memory ["-t", "<fn=2>\xf233</fn>  mem: <used>M (<usedratio>%)"] 20
                      , Run Date "<fn=2>\xf017</fn>  %b %d %Y - (%H:%M) " "date" 50
                        ],
         sepChar = "%",
         alignSep = "}{",
         template = "%StdinReader% | %cpu% | %memory% * %swap%  }{<fc=#ee9a00>%date%</fc> | %uname% | %CYVR% "
        }

