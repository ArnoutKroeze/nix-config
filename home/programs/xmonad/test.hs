import XMonad
import XMonad.Util.Run(spawnPipe)
import XMonad.Hooks.DynamicLog
import XMonad.Layout.IndependentScreens

import qualified XMonad.StackSet as W
import qualified Data.Map        as M
-- for polybar
import qualified Codec.Binary.UTF8.String              as UTF8
import qualified DBus                                  as D
import qualified DBus.Client                           as D
import           XMonad.Hooks.DynamicLog

main :: IO ()

main = do
    dbus <- D.connectSession
    D.requestName dbus (D.busName_ "org.xmonad.Log")
      [D.nameAllowReplacement, D.nameReplaceExisting, D.nameDoNotQueue]

    xmonad
      $ myConfig 

-- defautl terminal
myTerminal = "st"


-- wether focus follows the mouse pointer
myFocusFollowsMouse :: Bool
myFocusFollowsMouse = True

-- width of window borders
myBorderWidth = 1

-- modkey: mod4Mask is windows
myModMask = mod4Mask

-- name of workspaces
myWorkspaces    = withScreens 2 ["1","2","3","4","5","6","7","8","9"]

myNormalBorderColor  = "#dddddd"
myFocusedBorderColor = "#ff0000"

myKeys conf@(XConfig {XMonad.modMask = modkey}) = M.fromList $

    -- kill window
    [ ((modkey,			xK_q	), kill)

    -- movements
    , ((modkey, 		xK_j	), windows W.focusDown)
    , ((modkey, 		xK_k	), windows W.focusUp)

    , ((modkey .|. shiftMask, 	xK_j	), windows W.swapDown)
    , ((modkey .|. shiftMask, 	xK_k	), windows W.swapUp)

    -- shrink and expand master window
    , ((modkey, 		xK_h	), sendMessage Shrink)
    , ((modkey, 		xK_l	), sendMessage Expand)
    ]
    -- switching workspaces
    -- mod (+ shift) N   to switch (move to) workspace N
    ++
    
    -- move window to workspace N on same screen
    [((m .|. modkey, k), windows $ onCurrentScreen f i)
        | (i, k) <- zip (workspaces' conf) [xK_1 .. xK_9]
        , (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]]

    ++
    -- move windows between screens with 'i' and 'o'
    [((m .|. modkey, k), screenWorkspace sc >>= flip whenJust (windows . f))
        | (k, sc) <- zip [xK_i, xK_o] [0..]
        , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]]
    
    ++
    -- move windows between screens with arrows
    [((m .|. modkey, k), screenWorkspace sc >>= flip whenJust (windows . f))
        | (k, sc) <- zip [xK_Left, xK_Right] [0..]
        , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]]


myMouseBindings (XConfig {XMonad.modMask = modkey}) = M.fromList $

    -- mod-button1, Set the window to floating mode and move by dragging
    [ ((modkey, button1), (\w -> focus w >> mouseMoveWindow w
                                       >> windows W.shiftMaster))

    -- mod-button2, Raise the window to the top of the stack
    , ((modkey, button2), (\w -> focus w >> windows W.shiftMaster))

    -- mod-button3, Set the window to floating mode and resize by dragging
    , ((modkey, button3), (\w -> focus w >> mouseResizeWindow w
                                       >> windows W.shiftMaster))

    -- you may also bind events to the mouse scroll wheel (button4 and button5)
    ]


-- layouts
myLayout = tiled ||| Mirror tiled ||| Full
  where
    -- default tiling algorithm partitions the screen into two panes
    tiled   = Tall nmaster delta ratio

    -- The default number of windows in the master pane
    nmaster = 1

    -- Default proportion of screen occupied by master pane
    ratio   = 1/2

    -- Percent of screen to increment by when resizing panes
    delta   = 3/100


-- Polybar settings (needs DBus client).
--
mkDbusClient :: IO D.Client
mkDbusClient = do
  dbus <- D.connectSession
  D.requestName dbus (D.busName_ "org.xmonad.log") opts
  return dbus
 where
  opts = [D.nameAllowReplacement, D.nameReplaceExisting, D.nameDoNotQueue]

-- Emit a DBus signal on log updates
dbusOutput :: D.Client -> String -> IO ()
dbusOutput dbus str =
  let opath  = D.objectPath_ "/org/xmonad/Log"
      iname  = D.interfaceName_ "org.xmonad.Log"
      mname  = D.memberName_ "Update"
      signal = D.signal opath iname mname
      body   = [D.toVariant $ UTF8.decodeString str]
  in  D.emit dbus $ signal { D.signalBody = body }

polybarHook :: D.Client -> PP
polybarHook dbus =
  let wrapper c s | s /= "NSP" = wrap ("%{F" <> c <> "} ") " %{F-}" s
                  | otherwise  = mempty
      blue   = "#2E9AFE"
      gray   = "#7F7F7F"
      orange = "#ea4300"
      purple = "#9058c7"
      red    = "#722222"
  in  def { ppOutput          = dbusOutput dbus
          , ppCurrent         = wrapper blue
          , ppVisible         = wrapper gray
          , ppUrgent          = wrapper orange
          , ppHidden          = wrapper gray
          , ppHiddenNoWindows = wrapper red
          , ppTitle           = wrapper purple . shorten 90
          }

myPolybarLogHook dbus = myLogHook <+> dynamicLogWithPP (polybarHook dbus)



-- some hooks i dont really understand, meh
myEventHook = mempty

myLogHook = return ()

-- startup hook, maybe do some xset stuff
myStartupHook = do
    -- spawn "xrandr --auto --output DP-1 --mode 1920x1080 --left-of HDMI-1 &"
    spawn "autorandr -c"
    spawn "xset r rate 300 50"
    spawn "sshfs arnout@192.168.2.90:/mnt/hdd/ ~/test -p 2222"
    spawn "setxkbmap -option caps:super"
    spawn "xcape -e 'Super_L=Escape'"
    -- spawn "feh --bg-scale ~/media/wallpapers/nixos.png"




-- =<< statusBar myBar myPP toggleStrutsKey defaults

myBar = "xmobar ~/.config/nixpkgs/programs/xmonad/bar.hs"

myPP = xmobarPP { ppCurrent = xmobarColor "#429942" "" . wrap "<" ">" }

toggleStrutsKey XConfig {XMonad.modMask = modMask} = (modMask, xK_b)
  
 
myConfig = def {
    terminal	        = myTerminal,
    focusFollowsMouse   = myFocusFollowsMouse,
    borderWidth	        = myBorderWidth,
    modMask	      	= myModMask,
    workspaces 		= myWorkspaces,
    normalBorderColor	= myNormalBorderColor,
    focusedBorderColor	= myFocusedBorderColor,

    keys 		= myKeys,
    mouseBindings	= myMouseBindings,
      -- hooks, layouts
    layoutHook          = myLayout,
    -- manageHook          = myManageHook,
    handleEventHook     = myEventHook,
    logHook             = myLogHook,
    startupHook         = myStartupHook
    }

