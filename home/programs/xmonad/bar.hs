Config { font = "-*-Fixed-Bold-R-Normal-*-13-*-*-*-*-*-*-*"
       , additionalFonts = []
       , borderColor = "black"
       , border = TopB
       , bgColor = "black"
       , fgColor = "grey"
       , alpha = 255
       , position = TopW L 100
       , textOffset = -1
       , iconOffset = -1
       , lowerOnStart = True
       , pickBroadest = False
       , persistent = False
       , hideOnStart = False
       , iconRoot = "."
       , allDesktops = True
       , overrideRedirect = False
       , commands = [ Run Network "enp4s0" ["-L","0","-H","32",
                                            "--normal","green",
                                            "--high","red",
                                            "-S", "True",
                                            "-t", "<rx>|<tx>"
                                           ] 10
                    , Run Cpu ["-L","3","-H","50",
                               "--normal","green",
                               "--high","red"
                              ] 10
                    , Run Memory ["-L", "20", "-H", "80",
                                  "--low", "green",
                                  "--normal", "orange",
                                  "--high", "red",
                                  "-t","Mem: <usedratio>%"
                                 ] 10
                    , Run Com "uname" ["-n"] "" 36000
                    , Run Date "%a %b %_d %Y %H:%M:%S" "date" 10
                    , Run BatteryP ["BAT0"]
                                   [ "-t", "<acstatus>"
                                   , "-L", "20", "-H", "80"
                                   , "-l", "red", "-h", "green"
                                   , "--", "-O", "Charging: <left>%"
                                         , "-o", "Battery: <left>%"
                                         , "-i", "<fc=green>Fully Charged</fc>"
                                   ] 10
                    , Run ThermalZone 0 [] 30
                    , Run ThermalZone 1 [] 30
                    , Run Volume "default" "Master" [] 10
                    , Run Kbd [ ("us(altgr-intl)", "US")
                              , ("ru", "RU")
                              ]
                    , Run StdinReader
                    ]
       , sepChar = "%"
       , alignSep = "}{"
       , template = " %StdinReader%\
                    \ | %cpu%\
                    \ | %thermal0% %thermal1%\
                    \ | %memory%\
                    \ | %wlp2s0wi% %enp4s0%\
                    \ }{\
                    \ <fc=#ee9a00>%date%</fc>\
                    \ | %uname%\
                    \ | %default:Master%\
                    \ | <fc=#8080ff>%kbd%</fc>\
                    \ | %battery%"
}
