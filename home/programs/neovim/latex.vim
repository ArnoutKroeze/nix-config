autocmd VimLeave *.tex !texclear %

let g:vimtex_quickfix_autoclose_after_keystrokes = 1
