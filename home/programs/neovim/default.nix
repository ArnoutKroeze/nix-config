{ config, lib, pkgs, ...}:

let

  plugins = pkgs.vimPlugins;

  myVimPlugins = with plugins; [
    coc-nvim
    coc-yank      # yank plugin for CoC
    coc-pyright
    coc-vimtex
    gruvbox-community
    dracula-vim
    vim-airline
    nerdtree
    vim-commentary
    vim-nix
    vim-commentary
    vimtex

  ];

  baseConfig = builtins.readFile ./config.vim;
  cocConfig = builtins.readFile ./coc.vim;
  cocSettings   = builtins.toJSON (import ./coc-settings.nix);
  latexConfig = builtins.readFile ./latex.vim;
  vimConfig = baseConfig + cocConfig + latexConfig;

in 
{
  programs.neovim = {
    enable = true;
    extraConfig = vimConfig;
    plugins = myVimPlugins;
    withNodeJs = true; # for coc.nvim
    
  };

  xdg.configFile = {
    "nvim/coc-settings.json".text = cocSettings;
  };


}



