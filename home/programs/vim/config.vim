let mapleader=','
set number relativenumber "numbering on left
set encoding=utf-8
" set viewoptions=folds,options,cursor,unix,slash

colorscheme dracula 
set termguicolors " true colour support
set cursorline " hightlight line which cursor is on
" no background
hi Normal guibg=NONE ctermbg=NONE 
syntax on " syntax highlighting for different languages

set mouse=a " use mouse in vim
set clipboard=unnamedplus " use system clipboard
set expandtab " tabs become spaces
set scrolloff=5 " keeps cursor from top/bottom screen
" set incsearch " jump to first match automagically

set undodir=~/.local/share/nvim/undodir
set undofile
set noswapfile

" clear search highlighting
nnoremap <C-z> :nohlsearch<CR>

" disable ex-mode
nnoremap Q <Nop>
nnoremap gQ <Nop>

" Save file as sudo on files that require root permission
cnoremap w!! execute 'silent! write !sudo tee % >/dev/null' <bar> edit!

" autocomplete
"

" python
" run python on file with F9
autocmd FileType python map <buffer> <F9> :w<CR>:exec '!python3' shellescape(@%, 1)<CR>
autocmd FileType python imap <buffer> <F9> <esc>:w<CR>:exec '!python3' shellescape(@%, 1)<CR>

"  Nerdtree
nnoremap <leader>n :NERDTreeFocus<CR>
nnoremap <C-n> :NERDTree<CR>
nnoremap <C-t> :NERDTreeToggle<CR>
