{

  programs.newsboat = {
    enable = true;
    autoReload = true;

    urls = [
      {
        title = "abacus";
        url = "https://abacus.utwente.nl/en/news.atom";
        tags = ["enschede"];
      }
      {
        url = "---tech---";
      }
      {
        title = "dcnnt releases";
        url = "https://github.com/cyanomiko/dcnnt-py/tags.atom";
        tags = ["maintainer"];
      }
      {
        title = "nixos releases";
        url = "https://nixos.org/blog/announcements-rss.xml";
        tags = ["software"];
      }
      {
        title = "tally";
        url = "https://github.com/thewouter/tally/commits.atom";
        tags = ["software"];
      }
    ];
    extraConfig = ''
      # keybindings
        bind-key j down
        bind-key k up
        bind-key j next articlelist
        bind-key k prev articlelist
        bind-key J next-feed articlelist
        bind-key K prev-feed articlelist
        bind-key G end
        bind-key g home
        bind-key d pagedown
        bind-key u pageup
        bind-key l open
        bind-key h quit
        bind-key a toggle-article-read
        bind-key n next-unread
        bind-key N prev-unread
        bind-key D pb-download
        bind-key U show-urls
        bind-key x pb-delete

      # colours 
        color listnormal cyan default
        color listfocus black yellow standout bold
        color listnormal_unread cyan default bold
        color listfocus_unread yellow default bold
        color info red default bold
        color article white default bold

      # highlighs
        highlight all "---.*---" yellow
        highlight feedlist ".*(0/0))" black
        highlight article "(^Feed:.*|^Title:.*|^Author:.*)" cyan default bold
        highlight article "(^Link:.*|^Date:.*)" default default
        highlight article "http?://[^ ]+" green default
        highlight article "^(Title):.*$" red default
        highlight article "\\[[0-9][0-9]*\\]" magenta default bold
        highlight article "\\[image\\ [0-9]+\\]" green default bold
        highlight article "\\[embedded flash: [0-9][0-9]*\\]" green default bold
        highlight article ":.*\\(link\\)$" cyan default
        highlight article ":.*\\(image\\)$" cyan default
        highlight article ":.*\\(embedded flash\\)$" magenta default

      # macros


      '';
  };
}
