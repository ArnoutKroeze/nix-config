{ pkgs, ...}:

{
  programs.mpv = {
    enable = true;
    scripts = with pkgs.mpvScripts; [
      # thumbnail
      autoload
      sponsorblock
    ];
    
    # needed for thumbnails
    # config = {
    #   osc = "no";
    # };
  }; 
}

