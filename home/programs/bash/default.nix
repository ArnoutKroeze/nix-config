{ pkgs, ... }:

{ programs.bash = {
  enable = false;
  shellAliases = {
    sudo = "sudo ";
    e = "nvim";
    sdn = "shutdown -h 0";
  };

  initExtra = ''
    set -o vi  # enable vi-like control

    prompt_symbol=$(test "$UID" == "0" && echo "$RED#$NO_COLOR" || echo "$")

    git_prompt_path=${pkgs.git}/share/bash-completion/completions/git-prompt.sh
    if [ -f "$git_prompt_path" ] && ! command -v __git_ps1 > /dev/null; then
      source "$git_prompt_path"
    fi

    if [ -n "$VIRTUAL_ENV" ]; then
      env=$(basename "$VIRTUAL_ENV")
      export PS1="($env) $PS1"
    fi

    if [ -n "$IN_NIX_SHELL" ]; then
      export PS1="(nix-shell) $PS1"
    fi

    # Change dir with Fuzzy finding
    '';
  };
}
