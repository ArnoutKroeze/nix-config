{config, pkgs, lib, ... }:

{

  home.packages = with pkgs; [
    (st.overrideAttrs (oldAttrs: rec {
      src = fetchFromGitLab {
        owner = "ArnoutKroeze";
        repo = "st";
        rev = "a30531f7ac3b2b1c04459f353439cd4faeeb469b";
        sha256 = "0g0blsk905za240w7alny9d573qvm30h3m3nk4i72d42cmazdwfw";
      };
    }))
  ];
}


