{ pkgs, ...}:

{
  programs.beets = {
    enable = true;

    settings = {
      directory = "/media/muziek";
      library = "~/.local/share/beets/library.db";

      original_date = "yes";
      per_disc_numbering = "no";
      from_scratch = "yes";
      };
       # ui:
       #  color: yes
       #  colors:
       #    text_success: green
       #    text_warning: yellow
       #    text_error: red
       #    text_highlight: red
       #    text_highlight_minor: lightgray
       #    action_default: turquoise
       #    action: blue

      # musicbrainz:
       #  host: musicbrainz.com
       #  https: yes
 }; 
}

