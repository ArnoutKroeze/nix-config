{ pkgs, ... }:

{
  programs.ncmpcpp = {
    enable = true;

    package = pkgs.ncmpcpp.override { visualizerSupport = true; };

    # already given by mpd

    mpdMusicDir = /media/muziek;
    
    bindings = [
      { key = "+"; command = "show_clock"; }
      { key = "="; command = "volume_up"; }

      { key = "j"; command = "scroll_down"; }
      { key = "k"; command = "scroll_up"; }


      { key = "h"; command = "previous_column"; }
      { key = "l"; command = "next_column"; }

      { key = "."; command = "show_lyrics"; }
      
      { key = "n"; command = "next_found_item"; }
      { key = "N"; command = "previous_found_item"; }
      
      { key = "x"; command = "delete_playlist_items"; }
    ];

    settings = {
      lyrics_directory = "~/.local/share/lyrics";
      autocenter_mode = "yes";
      ignore_leading_the = "yes";

      default_place_to_search_in = "database";
      playlist_editor_display_mode = "columns";
      search_engine_display_mode = "columns";
      browser_display_mode = "columns";
      playlist_display_mode = "columns";
      colors_enabled = "yes";

      main_window_color = "white";
      header_window_color = "cyan";
      volume_color = "green";
      statusbar_color = "white";
      progressbar_color = "cyan";
      progressbar_elapsed_color = "white";

      song_columns_list_format = "(10)[red]{l} (30)[green]{t} (30)[magenta]{a} (30)[yellow]{b}";
      song_list_format = "{$7%a - $9}{$5%t$9}|{$5%f$9}$R{$6%b $9}{$3%l$9}";

      current_item_prefix = "$(red)$r";
      current_item_suffix = "$/r$(end)";
      current_item_inactive_column_prefix = "$(cyan)$r";
      current_item_inactive_column_suffix = "$/r$(end)";

      user_interface = "alternative";
      alternative_header_first_line_format = "$0$aqqu$/a {$6%a$9 - }{$3%t$9}|{$3%f$9} $0$atqq$/a$9";
      alternative_header_second_line_format = "{{$4%b$9}{ [$8%y$9]}}|{$4%D$9}";

      song_status_format = " $6%a $7⟫⟫ $3%t $7⟫⟫ $4%b ";

      visualizer_fifo_path = "/tmp/mpd.fifo";
      visualizer_output_name = "my_fifo";
      visualizer_sync_interval = "60";
      visualizer_type = "ellipse";
      visualizer_in_stereo = "no";
      visualizer_look = "◆▋";
      visualizer_spectrum_dft_size = 2;
      visualizer_spectrum_hz_max = 9500;

      cyclic_scrolling = "yes";
      header_text_scrolling = "yes";
      jump_to_now_playing_song_at_start = "yes";
      lines_scrolled = "2";

      incremental_seeking = "yes";
      seek_time = "1";

      header_visibility = "yes";
      statusbar_visibility = "yes";
      titles_visibility = "yes";

      progressbar_look =  "=>-";
      now_playing_prefix = "> ";
      centered_cursor = "yes";


      system_encoding = "utf-8";
      regular_expressions = "extended";

      empty_tag_marker = "";
      display_bitrate = "yes";
      enable_window_title = "yes";
      media_library_primary_tag = "album_artist";
    };
  };
}

