{ pkgs, ... }:

{
  programs.firefox = {
    enable = true;

     profiles = {
       default = {
         id = 0;
         settings = {
           "app.normandy.first_run" = false;
           "app.shield.optoutstudies.enabled" = true;
           "toolkit.telemetry.enabled" = false;

           "browser.contentblocking.category" = "strict"; 
           "browser.ctrlTab.recentlyUsedOrder" = false;
           "browser.link.open_newwindow" = true;
           "browser.search.widget.inNavBar" = true;
           "browser.urlbar.placeholderName" = "DuckDuckGo";
           "privacy.donottrackheader.enabled" = true;
           "privacy.trackingprotection.fingerprinting.enabled" = true;
           "privacy.trackingprotection.cryptomining.enables" = true;
           "privacy.firstpary.isolate" = true;
           "privacy.trackingprotection.enabled" = true;
           "geo.enabled" = false;
           "dom.event.clipboardevents.enabled" = false;
         };
       };
     };
  };
}


