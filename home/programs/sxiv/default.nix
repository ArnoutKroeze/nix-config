{ pkgs, ... }:

{
  home.packages = [pkgs.sxiv];

  xdg.configFile.sxiv = {
    # source = ./key-handler;
    target = "./sxiv/exec/key-handler";
    executable = true;
    text = ''
      #!/usr/bin/env sh
      while read file
      do
        case "$1" in
          "y")
              ${pkgs.xclip}/bin/xclip -selection clipboard -t image/png -i "$file";;
          "a")
              ${pkgs.dunst}/bin/dunstify hoi;;
        esac
      done
    '';
  };
}
