{ pkgs, ... }:

{
  gtk = {
    enable = true;

    iconTheme = {
      package = pkgs.papirus-icon-theme;
      name = "Papirus-Dark";
    };

    # theme = {
    #   package = pkgs.juno-theme;
    #   name = "Juno-palenight";
    # };

    theme = {
      package = pkgs.arc-theme;
      name = "Arc-Dark";
    };

    gtk3.extraConfig = {
      gtk-application-prefer-dark-theme = true;
    };

    # gtk4.extraConfig = {
    #   gtk-application-prefer-dark-theme = true;
    # };
  };

  qt = {
    enable = true;
    platformTheme = "gtk";
  };
}
