{ config, lib, pkgs, stdenv, ...}:

let

  # nice packages! <3
  defaultPkgs = with pkgs; [
    abook
    alsa-utils
    bc # terminal calculator
    brightnessctl
    cifs-utils # mount samba
    htop
    dcnnt
    discord
    dmenu
    dwarf-fortress
    dwarf-therapist
    feh
    ffmpeg
    firefox
    freetube
    gnucash
    glib # for flameshot
    keepassxc
    killall
    libreoffice
    libnotify # for flameshot
    mpd # music player daemon
    mpc_cli # mpd controller
    neofetch # i use nix btw
    newsboat # rss reader
    nitrogen # set wallpaper
    nyancat # just run it
    # openttd
    openttd-jgrpp # openttd with QOL improvements
    texlive.combined.scheme-full # LaTeX!!1!
    thunderbird # email client
    tldr # short help messages
    sl # choo choo motherfucker
    pamixer # pulseaudio commandline mixer
    pcmanfm # file manager
    protonup # for custom proton, run protonup to update
    pulsemixer # pulseaudio TUI

    (let 
      my-python-packages = python-packages: with python-packages; [
        requests
        pyautogui
      ];
      python-with-my-packages = python3.withPackages my-python-packages;
    in
    python-with-my-packages)

    ranger # tui file manager
    rclone
    spotify-tui
    sshfs # connects to pi
    vorbis-tools
    wget
    yt-dlp # download videos
    xclip # at least needed for neovim to use system clipboard
    xdotool
    xorg.xrandr
    zathura # pdf reader
    (pkgs.callPackage ./local-packages/oggify/default.nix {})
    # (pkgs.callPackage ./local-packages/calcure/default.nix {})
  ]; 
  

  fonts = with pkgs; [
    liberation_ttf # normal font
    dejavu_fonts
    font-awesome
    nerdfonts # polybar
    iosevka # polybar
    noto-fonts # needed for fallback
    noto-fonts-cjk
    noto-fonts-emoji
    fira-code
    fira-code-symbols
    # mplus-outline-fonts
    material-design-icons
  ];

  archives = with pkgs; [
    unzip
    zip
  ];

in
{
  programs.home-manager.enable = true;

  nixpkgs.overlays = [
    (import ./overlays/polybar)
    (import ./overlays/proton-ge)
  ];



  nixpkgs.config = {
    allowUnfree = true;
  };

  imports = (import ./programs)
  ++ (import ./services) 
  ++ [(import ./themes)];
  # ++ (import ./scripts);



  xdg = {
    enable = true;
    userDirs = {
      documents = "$HOME/documents";
      download = "$HOME/downloads";
      music = "$HOME/muziek";
    };
  };

  xsession = {
    enable = true;
    numlock.enable = true;
    initExtra = "
      autorandr -c
      $HOME/.config/polybar/launch.sh
      xset r rate 300 50 &
      sxhkd &
      dcnnt start

      # set caps as escape
      setxkbmap -option caps:super
      killall xcape 2>/dev/null ; xcape -e 'Super_L=Escape'

    ";
  };

  
  home = {
    username = "arnout";
    homeDirectory = "/home/arnout";

    # You can update home manager without changing this,
    # See home manager release notes
    stateVersion = "21.11";

    packages = defaultPkgs ++ fonts ++ archives ;

    sessionPath = ["/home/arnout/.local/bin"];

    sessionVariables = {
      EDITOR = "nvim";
    };

    shellAliases = {
      sdn = "shutdown -h 0";
      sudo = "sudo ";
      hc = "nvim $HOME/.nix-config/home/home.nix";
      hs = "user-upgrade";
      sc = "nvim $HOME/.nix-config/system/desktop/configuration.nix";
      ss = "system-upgrade";
      nsp = "nix-shell -p ";

    };
    pointerCursor = {
      x11.enable = true;
      size = 27;
      package = pkgs.capitaine-cursors;
      name = "capitaine-cursors";
    };


  };

  systemd.user.startServices = "sd-switch";

  programs.git = {
    enable = true;
    userEmail = "blank";
    userName = "Arnout Kroeze";
  };

  services = {
    dunst.enable = true;
    easyeffects.enable = true;
    flameshot.enable = true;
    unclutter.enable = true;
    clipmenu.enable = true;
    syncthing.enable = true;
    playerctld.enable = true;
    kdeconnect.enable = true;
    spotifyd.enable = true;
  };

  xdg.mimeApps = {
    enable = true;
    # associations.added = {
      # "application/pdf" = ["org.pwmt.zathura-pdf-mupdf.desktop"];
    # };
    defaultApplications = {
      "application/pdf" = ["org.pwmt.zathura-pdf-mupdf.desktop"];
    };
  };
}
