{ config, pkgs, ... }:

{

  # Use the systemd-boot EFI boot loader.
  # boot.loader.systemd-boot.enable = true;
  # boot.loader.efi.canTouchEfiVariables = true;


  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      ../general.nix
    ];


  boot = {
    loader.grub = {
      enable = true;
      version = 2;
      efiSupport = true;
      enableCryptodisk = true;
      device = "nodev";
    };

    initrd = {
      luks.devices = {
        crypted = {
          device = "/dev/disk/by-uuid/6c188146-d9df-4530-8c7e-42cab6e9157f";
          preLVM = true;
        };
      };

      preLVMCommands = ''
        echo '            --- ATTENTIE ---'
        echo '    Deze laptop is van Arnout Kroeze '
        echo '    gevonden? Mail info at arnoutkroeze.nl'
        echo '            --- ATTENTIE ---'
      '';
    };
    plymouth.enable = true;
  };

  networking = {
    hostName = "flappie";
    networkmanager.enable = true;
    wireless.dbusControlled = true;

    interfaces = {
      enp0s31f6.useDHCP = true;
      wlp4s0.useDHCP = true;
    };
  };



  # Enable CUPS to print documents.
  services.printing.enable = true;
  services.avahi.enable = true;
  services.avahi.nssmdns = true;

 # Enable touchpad support (enabled default in most desktopManager).
  services.xserver.libinput.enable = true;
  services.upower.enable = true;

}

