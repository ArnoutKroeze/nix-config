# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

# Desktop specific config
#
#
{config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      ../general.nix
    ];

  fileSystems."/media/share" = {
    device = "//192.168.2.90/samba-share";
    fsType = "cifs";
    options = let
      automount_opts = "x-systemd.automount,noauto,uid=1000,gid=100,x-systemd.idle-timeout=60,x-systemd.device-timeout=5s,x-systemd.mount-timeout=5s";
    
    in ["${automount_opts}"];
  };

  fileSystems."/media/muziek" = {
    device = "//192.168.2.90/muziek";
    fsType = "cifs";
    options = let
      automount_opts = "x-systemd.automount,noauto,uid=1000,gid=100,x-systemd.idle-timeout=60,x-systemd.device-timeout=5s,x-systemd.mount-timeout=5s";
    
    in ["${automount_opts}"];
  };
  
  # Use the GRUB 2 boot loader.
  boot.loader = {
    systemd-boot.enable = true;
    efi = {
      canTouchEfiVariables = true;
    };
  };
  
  networking = {
    hostName = "home"; # Define your hostname.
  };

  services.xserver.videoDrivers = [ "nvidia" ];

  programs.steam.enable = true;

}

