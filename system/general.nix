{pkgs, ...}:

{
  fonts.fonts = with pkgs; [
    # nerdfonts
    font-awesome
    # dejavu_fonts
    # material-design-icons
  ];

  nix = {
    package = pkgs.nixFlakes;
    extraOptions = ''
      experimental-features = nix-command flakes
    '';
  };

  time.timeZone = "Europe/Amsterdam";

  networking.firewall.enable = false;

   i18n.defaultLocale = "nl_NL.UTF-8";
   services.openssh.enable = true;

  services.xserver = {
    enable = true;
    libinput.enable = true;

    displayManager = {
      defaultSession = "none+dwm";
      lightdm = {
        enable = true;
        background = ./mandelbrot.png;
        greeters.pantheon = {
          # draw-user-backgrounds = true;
          enable = true;
        };
      };
      sessionCommands = "
        autorandr -c
        xset r rate 300 50 &
        sxhkd &
        ~/.config/polybar/launch.sh &
        dcnnt start
      ";
    };

    windowManager.dwm.enable = true;
    xkbOptions = "caps:super";
  };

  sound.enable = false;

  services.pipewire = {
    enable = true;
    pulse.enable = true;
    alsa.enable = true;
  };

  security.rtkit.enable = true;


  nixpkgs.overlays = [
    (self: super: {
      dwm = super.dwm.overrideAttrs (oldAttrs: rec {
        src = super.fetchFromGitLab {
          owner = "arnoutkroeze";
          repo = "dwm";
          rev = "95459281213fd37f61c1250af4c4d7df3ce7653b";
          sha256 = "sha256-PYsuWMzTq8HeNj7xxhxE/kynPyQHVSO9N0XNpTTt/Qg=";
        };
        buildInputs = oldAttrs.buildInputs ++ [super.yajl];
      });
    })
    (self: super: {
      slock = super.slock.overrideAttrs (oldAttrs: rec {
        patches = [
          (super.fetchpatch {
            url = "http://tools.suckless.org/slock/patches/unlock_screen/slock-unlock_screen-1.4.diff";
            sha256 = "sha256-uUBpRiMEkrjIkRQQwtswuPBG35L2Pxt4RgIrWqZ0b74=";
          })
        ];
      });
    })
  ];

 


  # virtialbox support
  virtualisation.virtualbox.host.enable = true;
  users.extraGroups.vboxusers.members = [ "arnout" ];

  users.users.arnout = {
    isNormalUser = true;
    extraGroups = [ "audio" "wheel" ]; # Enable ‘sudo’ for the user.
  };

  environment.systemPackages = with pkgs; [
    neovim
    lxqt.lxqt-policykit # for samba authentication
    sxhkd
    networkmanager
    xcape
  ];


  # put ~/.local/bin in path
  environment.localBinInPath = true;

  programs.zsh.enable = true;
  users.users.arnout.shell = pkgs.zsh;

  # for sshfs / samba 
  services.gvfs.enable = true;

  programs.kdeconnect.enable = true;
  programs.slock.enable = true;

  # otherwise home manager complains
  programs.dconf.enable = true;

  nixpkgs.config.allowUnfree = true;
  system.stateVersion = "22.05"; # Did you read the comment?

}
