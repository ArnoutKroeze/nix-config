# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  fileSystems."/mnt/share" = {
    device = "//192.168.2.90/samba-share";
    fsType = "cifs";
    options = let
      automount_opts = "x-systemd.automount,noauto,uid=1000,gid=100,x-systemd.idle-timeout=60,x-systemd.device-timeout=5s,x-systemd.mount-timeout=5s";
    
    in ["${automount_opts}"];
  };

  fileSystems."/mnt/muziek" = {
    device = "//192.168.2.90/muziek";
    fsType = "cifs";
    options = let
      automount_opts = "x-systemd.automount,noauto,uid=1000,gid=100,x-systemd.idle-timeout=60,x-systemd.device-timeout=5s,x-systemd.mount-timeout=5s";
    
    in ["${automount_opts}"];
  };
  
  # Use the GRUB 2 boot loader.
  boot.loader = {
    efi = {
      efiSysMountPoint = "/boot/efi";
      canTouchEfiVariables = true;
    };
    grub = {
      efiSupport = true;
      device = "nodev";
    };
  };

  # boot.kernelPackages = pkgs.linuxPackages_latest;

  fonts.fonts = with pkgs; [
    nerdfonts
    font-awesome
    dejavu_fonts
    material-design-icons
  ];

  time.timeZone = "Europe/Amsterdam";

  networking = {
    hostName = "nixos"; # Define your hostname.
    firewall.enable = false;

    interfaces.enp4s0.useDHCP = true;
  };


  # Select internationalisation properties.
   i18n.defaultLocale = "nl_NL.UTF-8";

   services.openssh.enable = true;

  # Enable the X11 windowing system.
  services.xserver = {
    enable = true;
    libinput.enable = true;

    displayManager = {
      defaultSession = "none+dwm";
      lightdm = {
        enable = true;
        greeters.enso = {
          enable = true;
        };
      };
      sessionCommands = "
        autorandr -c
        xset r rate 300 50 &
        sxhkd &
        ~/.config/polybar/launch.sh &
        dcnnt start
      ";
    };

    # desktopManager.gnome.enable = true;
    # desktopManager.xfce.enable = true;


    windowManager.qtile.enable = true;
    windowManager.dwm.enable = true;
    xkbOptions = "caps:super";
    videoDrivers = [ "nvidia" ];
  };

  # Enable sound.
  sound.enable = false;
  # hardware.pulseaudio = {
    # enable = true;
  # };

  services.pipewire = {
    enable = true;
    pulse.enable = true;
    alsa.enable = true;
  };

  security.rtkit.enable = true;

  nixpkgs.overlays = [
    (self: super: {
      dwm = super.dwm.overrideAttrs (oldAttrs: rec {
        src = super.fetchFromGitLab {
          owner = "arnoutkroeze";
          repo = "dwm";
          rev = "95459281213fd37f61c1250af4c4d7df3ce7653b";
          sha256 = "sha256-PYsuWMzTq8HeNj7xxhxE/kynPyQHVSO9N0XNpTTt/Qg=";
        };
        buildInputs = oldAttrs.buildInputs ++ [super.yajl];
      });
    })
  ];


  users.users.arnout = {
    isNormalUser = true;
    extraGroups = [ "audio" "wheel" ]; # Enable ‘sudo’ for the user.
  };

  environment.systemPackages = with pkgs; [
    neovim
    lxqt.lxqt-policykit # for samba authentication
    sxhkd
    xcape
  ];

  environment.localBinInPath = true;

  programs.steam.enable = true;

  programs.zsh.enable = true;
  users.users.arnout.shell = pkgs.zsh;


  # for sshfs / samba 
  services.gvfs.enable = true;

  # otherwise home-manager gives an error
  programs.dconf.enable = true;
  programs.kdeconnect.enable = true;


  nixpkgs.config.allowUnfree = true;
  system.stateVersion = "21.11"; # Did you read the comment?

}

